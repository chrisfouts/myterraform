/* This is a multi-line comment. This is a multi-line comment.
This is a multi-line comment. This is a multi-line comment. This
is a multi-line comment. This is a multi-line comment. */
provider "aws" {
  region = var.region
}
# This is a single-line comment.
resource "aws_instance" "base" {
  ami           = "ami-408c7f28"
  instance_type = "t1.micro"
}

resource "aws_eip" "base" {
  instance = aws_instance.base.id
}
